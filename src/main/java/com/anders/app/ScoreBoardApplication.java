package com.anders.app;

import com.anders.app.model.ScoreBoardModel;

import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScoreBoardApplication {
	
	protected ArrayList<ScoreBoardModel> scoreList = new ArrayList<>();

	public static void main(String[] args) {
		SpringApplication.run(ScoreBoardApplication.class, args);
		
	}

}
