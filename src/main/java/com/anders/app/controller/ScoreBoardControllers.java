package com.anders.app.controller;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.apache.commons.collections4.IteratorUtils;

import com.anders.app.interfaces.ScoreRepository;
import com.anders.app.model.ScoreBoardModel;

@Controller
public class ScoreBoardControllers{
	
	@Autowired
	HttpServletRequest Request;
	
	@Autowired
	private ScoreRepository repository;
	
	public boolean populateWebsite(Model view)
	{
		Iterator<ScoreBoardModel> playerList = repository.findAll().iterator();
		List<ScoreBoardModel> playerArrayList = IteratorUtils.toList(playerList);
		Collections.sort(playerArrayList);
		view.addAttribute("stats", playerArrayList);
		return true;
	}

	@GetMapping("/")
	public String homePage(Model view)
	{
		populateWebsite(view);
		return "home";
	}
	
	
	// add score to the internal database for processing
	@PostMapping("/score")
	public String addScore(Model view)
	{
		// Get the parameter posted by the user
	    String playerName = Request.getParameter("playerName");
		int playerScore = Integer.parseInt(Request.getParameter("playerScore"));
		
		if(!playerName.isEmpty() && playerScore != 0)
		{
			if(repository.findByName(playerName) != null)
				repository.deleteById(repository.findByName(playerName).getId());
			
			repository.save(new ScoreBoardModel(playerName, playerScore));
		}
		return homePage(view);
	}
	
	
}
