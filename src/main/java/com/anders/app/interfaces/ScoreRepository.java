package com.anders.app.interfaces;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.anders.app.model.ScoreBoardModel;

@Repository
public interface ScoreRepository  extends CrudRepository<ScoreBoardModel, Long>
{
	ArrayList<ScoreBoardModel> findByScore(int score);
	ArrayList<ScoreBoardModel> findAll();
	ScoreBoardModel findByName(String name);

}
