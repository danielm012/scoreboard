package com.anders.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


// The class will serve as the data structure
@Entity
@Table(name = "Players")
public class ScoreBoardModel implements Comparable<ScoreBoardModel>{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	private int score;
	
	protected ScoreBoardModel() {}
	
	public ScoreBoardModel(String name, int score)
	{
		this.name = name;
		this.score = score;
	}
	
	public Long getId()
	{
		return id;
	}
	
	/**
	 * The module will get the current player name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name will set the score for the player
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Get the current player score
	 */
	public Integer getScore() {
		return score;
	}
	/**
	 * Set the current player score
	 */
	public void setScore(int score) {
		this.score = score;
	}

	@Override
	public int compareTo(ScoreBoardModel foreignObject) {
		return this.getScore().compareTo(foreignObject.getScore()) * -1;
	}
		

}
